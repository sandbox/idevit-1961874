<?php

/**
 * @file
 * Here we create the admin page.
 */

/**
 * Callback Functions, Forms, and Tables
 */

/**
 * Settings form for color index.
 */
function coloring_admin() {
  $form['coloring_samples'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of sample points'),
    '#default_value' => variable_get('coloring_samples', 25),
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t("The number of samples to take from an image. These are calculated to square numbers so 27 becomes 25."),
    '#required' => TRUE,
  );
  $form['coloring_delta'] = array(
    '#type' => 'textfield',
    '#title' => t('Quantization delta'),
    '#default_value' => variable_get('coloring_delta', 24),
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t("Allow you to select the quantization delta. The smaller the delta the more accurate the color. This also increases the number of similar colors."),
    '#required' => TRUE,
  );
  $form['color_index']['reduce_brightness'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reduce brightness.'),
    '#default_value' => variable_get('coloring_brightness', TRUE),
    '#description' => t("Reduce brightness variants of the same color."),
  );
  $form['color_index']['reduce_gradients'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reduce gradient.'),
    '#default_value' => variable_get('coloring_gradient', 0),
    '#description' => t("Reduces gradient variants ( useful for logos )."),
  );
  $form['coloring_search_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Color Search Options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['coloring_search_options']['color_search_options'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable color naming/rounding.'),
    '#default_value' => variable_get('coloring_search_colorname', 0),
    '#description' => t("Disables color names, but leaves hexcode more accurate."),
  );

  $content = field_info_instances();
  $content_type = array();
  $field_prop = array();
  $props = array();
  foreach ($content['node'] as $key => $contents) {
    $formatter = 0;
    $field_images = array();
    $content_image = array();
    foreach ($content['node'][$key] as $field_prop) {
      if ($field_prop['widget']['module'] == 'image') {
        $content_image[$key][$field_prop['field_name']] = $field_prop['field_name'];
        $field_images[] = $field_prop;
      }
      if ($field_prop['display']['default']['module'] == 'coloring_palette_formatter') {
        $formatter++;
      }
      $count = count($field_images);
      if ($count >= 1 && $formatter >= 1) {
        $content_type[$key] = $key;
        // Use content type as index for the array.
        foreach ($field_images as $props) {
          if ($props['widget']['module'] == 'image') {
            $content_type[$key] = $content_image[$key];
          }
        }
      }
    }
  }

  $options = variable_get('coloring_content_types');

  foreach ($content_type as $key => $type) {
    $name = node_type_load($key);
    $cname = $name->name;
    $form['coloring_content_types'][$key] = array(
      '#type' => 'fieldset',
      '#title' => $cname . ' (' . $key . ')',
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['coloring_content_types'][$key]['fields'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Fields'),
      '#multiple' => TRUE,
      '#description' => t('Check the image fields to include in the color index'),
      '#options' => $content_type[$key],
      '#default_value' => $options[$key],
    );
  }
  $form['coloring_button']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#submit' => array('coloring_admin_settings_submit'),
  );
  return $form;
  // TODO use return system_settings_form($form);
}

/**
 * Form submission handler for coloring_admin.
 *
 * @see coloring_admin()
 */
function coloring_admin_settings_submit($form_state) {
  $content_type_array = array();

  $samples = $form_state['coloring_samples']['#value'];
  $delta = $form_state['coloring_delta']['#value'];
  $brightness = $form_state['color_index']['reduce_brightness']['#value'];
  $gradient = $form_state['color_index']['reduce_gradients']['#value'];
  $search_names = $form_state['coloring_search_options']['color_search_options']['#value'];
  $content_type = $form_state['coloring_content_types'];
  foreach ($content_type as $key => $type) {
    if (!empty($type['fields']['#value'])) {
      $content_type_array[$content_type[$key]['#parents'][0]] = $type['fields']['#value'];
    }
  }
  variable_set('coloring_samples', $samples);
  variable_set('coloring_delta', $delta);
  variable_set('coloring_brightness', $brightness);
  variable_set('coloring_gradient', $gradient);
  variable_set('coloring_search_colorname', $search_names);
  variable_set('coloring_content_types', $content_type_array);

  drupal_set_message(t('Settings have been saved'), 'status');
}
