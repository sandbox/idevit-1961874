  ___     _         _           
 / __|___| |___ _ _(_)_ _  __ _ 
| (__/ _ \ / _ \ '_| | ' \/ _` |
 \___\___/_\___/_| |_|_||_\__, |
                          |___/ 


Credits
============
This module is developed by iDEVit. As a training project and 
the fact that color indexing functionality did not exist in Drupal.
It's GPL v2 licensed and under active development.

Coloring
============
This module contains a module and several sub-modules.

module: 
	coloring
sub-modules:
	color_palette
	color_index
	color_search_helper

This module will index one or more pictures in your content type
 and display these in a table or palette formatter.

The color_palette submodule contains two display types for a text-
field. Color palette will make a nice palette with all the detected
colors next to one another. Color table will represent all colors 
in a square i.e. 25 colors will make 5 rows and 5 columns.

The color_index submodule does the indexing, it has an admin page, 
The admin page is located at admin/config/media/coloring. Here you 
can define how many colors you would like to index, the diversity 
of the colors, to reduce brightness or gradient and select which 
image fields are to be used. For example, when you index logo's or
other images with less than the amount of sample points.

The color_search_helper is a helping function which uses the module
jquery_colorpicker created by Jaypan. 

Installation
============

Extract Module
1. Go to your Drupal module folder, (sitename)/sites/all/modules, 
and extract the module.

Enable Module
2. In the browser, go to your website and enable the module at 
(sitename)/admin/modules. You can find it in the group Coloring.

Add Fields
3. To make use of the indexer you need to add a textfield named 
color so its system name is 'field_color' and give it the maximum
 length of "2048" characters, and set the number of values to 
"Unlimited". Next be sure to have an image field. If not, make one.

Set Display Format
4. Go to "Manage Display". Here set the "Format" for the text-
field to either "Color Table" or "Color palette". The module
checks if there is a color formatter on the textfield.

Enable Image Field(s)
5. Go to the admin page of this module at, 
(sitename)/admin/config/media/coloring Here you can adjust the
settings or leave these at default. On the bottom of the page 
check the boxes with image field names you wish to index.

Add Images, Save, Index!
6. The indexing is triggered on the save of the page. So you 
need to create a new page of the above modified content type, 
or edit existing. Don't forget to add image(s) ;) 

Search Helper
7. Enable the module color_search_helper to make use of the
handy colorpicker in the search page. You need to have the
jquery_colorpicker module enabled. The jquery_colorpicker 
uses the eyecon colorpicker libraries, download these at
http://www.eyecon.ro/colorpicker/ 

Limitations
============
The color_index module assumes only one textfield as described 
above where it utilizes multiple image fields as set in the 
admin page.

Usecase
============
This module is particularly handy for any website that has to do 
with images. For example web stores or photographer sites. 
The original idea is inspired on http://dribbble.com/colors 
